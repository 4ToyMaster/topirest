package com.example.topirest.api.controllers;


import com.example.topirest.api.urls.Url;
import com.example.topirest.dao.entity.TestResultItem;
import com.example.topirest.service.TestResultItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Результаты", description = "API для результатов")
@RestController(value = Url.TestResultItem.FULL)
public class TestResultItemController {

  @Autowired
  private TestResultItemService TestResultItemService;

  @ApiOperation(value = "найти всё")
  @RequestMapping(value = Url.TestResultItem.FULL + "/all", method = RequestMethod.GET)
  public List<com.example.topirest.dao.entity.TestResultItem> findAll() {
    return TestResultItemService.getAll();
  }

  @ApiOperation(value = "найти")
  @RequestMapping(value = Url.TestResultItem.FULL + "/find", method = RequestMethod.GET)
  public Optional<TestResultItem> find(@RequestParam Long id) {
    return TestResultItemService.findById(id);
  }

  @ApiOperation("Сохранить")
  @RequestMapping(value = Url.TestResultItem.FULL + "/save", method = RequestMethod.POST)
  public void save(@RequestBody TestResultItem TestResultItem) {
    TestResultItemService.save(TestResultItem);
  }

  @ApiOperation("Удалить")
  @RequestMapping(value = Url.TestResultItem.FULL + "/delete", method = RequestMethod.GET)
  public void delete(@RequestParam Long id) {
    TestResultItemService.deleteById(id);
  }

}
