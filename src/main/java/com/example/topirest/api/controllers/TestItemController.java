package com.example.topirest.api.controllers;


import com.example.topirest.api.urls.Url;
import com.example.topirest.dao.entity.TestItem;
import com.example.topirest.service.TestItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Тесты", description = "API для тестов")
@RestController(value = Url.TestItem.FULL)
public class TestItemController {

  @Autowired
  private TestItemService testItemService;

  @ApiOperation(value = "найти всё")
  @RequestMapping(value = Url.TestItem.FULL + "/all", method = RequestMethod.GET)
  public List<com.example.topirest.dao.entity.TestItem> findAll() {
    return testItemService.getAll();
  }

  @ApiOperation(value = "найти")
  @RequestMapping(value = Url.TestItem.FULL + "/find", method = RequestMethod.GET)
  public Optional<TestItem> find(@RequestParam Long id) {
    return testItemService.findById(id);
  }

  @ApiOperation("Сохранить")
  @RequestMapping(value = Url.TestItem.FULL + "/save", method = RequestMethod.POST)
  public void save(@RequestBody TestItem TestItem) {
    testItemService.save(TestItem);
  }

  @ApiOperation("Удалить")
  @RequestMapping(value = Url.TestItem.FULL + "/delete", method = RequestMethod.GET)
  public void delete(@RequestParam Long id) {
    testItemService.deleteById(id);
  }

}
