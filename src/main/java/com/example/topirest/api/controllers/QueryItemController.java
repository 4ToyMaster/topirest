package com.example.topirest.api.controllers;


import com.example.topirest.api.urls.Url;
import com.example.topirest.dao.entity.QueryItem;
import com.example.topirest.service.QueryItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Вопросы", description = "API для вопросов")
@RestController(value = Url.QueryItem.FULL)
public class QueryItemController {

  @Autowired
  private QueryItemService queryItemService;

  @ApiOperation(value = "найти всё")
  @RequestMapping(value = Url.QueryItem.FULL + "/all", method = RequestMethod.GET)
  public List<com.example.topirest.dao.entity.QueryItem> findAll() {
    return queryItemService.getAll();
  }

  @ApiOperation(value = "найти")
  @RequestMapping(value = Url.QueryItem.FULL + "/find", method = RequestMethod.GET)
  public Optional<QueryItem> find(@RequestParam Long id) {
    return queryItemService.findById(id);
  }

  @ApiOperation("Сохранить")
  @RequestMapping(value = Url.QueryItem.FULL + "/save", method = RequestMethod.POST)
  public void save(@RequestBody QueryItem QueryItem) {
    queryItemService.save(QueryItem);
  }

  @ApiOperation("Удалить")
  @RequestMapping(value = Url.QueryItem.FULL + "/delete", method = RequestMethod.GET)
  public void delete(@RequestParam Long id) {
    queryItemService.deleteById(id);
  }

}
