package com.example.topirest.api.controllers;


import com.example.topirest.api.urls.Url;
import com.example.topirest.dao.entity.PossibleAnswer;
import com.example.topirest.service.PossibleAnswerService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Ответы", description = "API для ответов на вопросы")
@RestController(value = Url.PossibleAnswer.FULL)
public class PossibleAnswerController {

  @Autowired
  private PossibleAnswerService possibleAnswerService;

  @ApiOperation(value = "найти всё")
  @RequestMapping(value = Url.PossibleAnswer.FULL + "/all", method = RequestMethod.GET)
  public List<com.example.topirest.dao.entity.PossibleAnswer> findAll() {
    return possibleAnswerService.getAll();
  }

  @ApiOperation(value = "найти")
  @RequestMapping(value = Url.PossibleAnswer.FULL + "/find", method = RequestMethod.GET)
  public Optional<PossibleAnswer> find(@RequestParam Long id) {
    return possibleAnswerService.findById(id);
  }

  @ApiOperation("Сохранить")
  @RequestMapping(value = Url.PossibleAnswer.FULL + "/save", method = RequestMethod.POST)
  public void save(@RequestBody PossibleAnswer PossibleAnswer) {
    possibleAnswerService.save(PossibleAnswer);
  }

  @ApiOperation("Удалить")
  @RequestMapping(value = Url.PossibleAnswer.FULL + "/delete", method = RequestMethod.GET)
  public void delete(@RequestParam Long id) {
    possibleAnswerService.deleteById(id);
  }

}
