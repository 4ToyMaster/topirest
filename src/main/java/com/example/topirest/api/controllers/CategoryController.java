package com.example.topirest.api.controllers;


import com.example.topirest.api.urls.Url;
import com.example.topirest.dao.entity.Category;
import com.example.topirest.service.CategoryService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Категории", description = "API для категорий")
@RestController(value = Url.Category.FULL)
public class CategoryController {

  @Autowired
  private CategoryService categoryService;

  @ApiOperation(value = "найти всё")
  @RequestMapping(value = Url.Category.FULL + "/all", method = RequestMethod.GET)
  public List<com.example.topirest.dao.entity.Category> findAll() {
    return categoryService.getAll();
  }

  @ApiOperation(value = "найти")
  @RequestMapping(value = Url.Category.FULL + "/find", method = RequestMethod.GET)
  public Optional<Category> find(@RequestParam Long id) {
    return categoryService.findById(id);
  }

  @ApiOperation("Сохранить")
  @RequestMapping(value = Url.Category.FULL + "/save", method = RequestMethod.POST)
  public void save(@RequestBody Category category) {
    categoryService.save(category);
  }

  @ApiOperation("Удалить")
  @RequestMapping(value = Url.Category.FULL + "/delete", method = RequestMethod.GET)
  public void delete(@RequestParam Long id) {
    categoryService.deleteById(id);
  }

}
