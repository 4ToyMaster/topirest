package com.example.topirest.api.controllers;


import com.example.topirest.api.urls.Url;
import com.example.topirest.dao.entity.LearningItem;
import com.example.topirest.service.LearningItemService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Учебные материалы", description = "API для уч. материалов")
@RestController(value = Url.LearningItem.FULL)
public class LearningItemController {

  @Autowired
  private LearningItemService learningItemService;

  @ApiOperation(value = "найти всё")
  @RequestMapping(value = Url.LearningItem.FULL + "/all", method = RequestMethod.GET)
  public List<com.example.topirest.dao.entity.LearningItem> findAll() {
    return learningItemService.getAll();
  }

  @ApiOperation(value = "найти материал")
  @RequestMapping(value = Url.LearningItem.FULL + "/find", method = RequestMethod.GET)
  public Optional<LearningItem> find(@RequestParam Long id) {
    return learningItemService.findById(id);
  }

  @ApiOperation("Сохранить")
  @RequestMapping(value = Url.LearningItem.FULL + "/save", method = RequestMethod.POST)
  public void save(@RequestBody LearningItem LearningItem) {
    learningItemService.save(LearningItem);
  }

  @ApiOperation("Удалить")
  @RequestMapping(value = Url.LearningItem.FULL + "/delete", method = RequestMethod.GET)
  public void delete(@RequestParam Long id) {
    learningItemService.deleteById(id);
  }

}
