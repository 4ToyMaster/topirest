package com.example.topirest.api.controllers;


import com.example.topirest.api.urls.Url;
import com.example.topirest.dao.entity.UserData;
import com.example.topirest.service.UserDataService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@Api(tags = "Пользователи", description = "API для пользователей")
@RestController(value = Url.UserData.FULL)
public class UserDataController {

  @Autowired
  private UserDataService UserDataService;

  @ApiOperation(value = "найти всё")
  @RequestMapping(value = Url.UserData.FULL + "/all", method = RequestMethod.GET)
  public List<com.example.topirest.dao.entity.UserData> findAll() {
    return UserDataService.getAll();
  }

  @ApiOperation(value = "найти")
  @RequestMapping(value = Url.UserData.FULL + "/find", method = RequestMethod.GET)
  public Optional<UserData> find(@RequestParam Long id) {
    return UserDataService.findById(id);
  }

  @ApiOperation("Сохранить")
  @RequestMapping(value = Url.UserData.FULL + "/save", method = RequestMethod.POST)
  public void save(@RequestBody UserData UserData) {
    UserDataService.save(UserData);
  }

  @ApiOperation("Удалить")
  @RequestMapping(value = Url.UserData.FULL + "/delete", method = RequestMethod.GET)
  public void delete(@RequestParam Long id) {
    UserDataService.deleteById(id);
  }

}
