package com.example.topirest.api.urls;

public interface Url {

  String ROOT = "api";

  interface Category {
    String PART = "category";
    String FULL = ROOT + "/" + PART;
  }

  interface LearningItem {
    String PART = "learningItem";
    String FULL = ROOT + "/" + PART;
  }

  interface PossibleAnswer {
    String PART = "answer";
    String FULL = ROOT + "/" + PART;
  }

  interface QueryItem {
    String PART = "query";
    String FULL = ROOT + "/" + PART;
  }

  interface TestItem {
    String PART = "test";
    String FULL = ROOT + "/" + PART;
  }

  interface TestResultItem {
    String PART = "result";
    String FULL = ROOT + "/" + PART;
  }

  interface UserData {
    String PART = "user";
    String FULL = ROOT + "/" + PART;
  }

}
