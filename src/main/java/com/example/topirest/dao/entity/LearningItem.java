package com.example.topirest.dao.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "learningItem")
public class LearningItem {
  @Id
  @GeneratedValue
  private Long id;

  private String name;

  @Type(type = "text")
  private String item;

  @ManyToOne
  @JoinColumn(name = "categoty_id", nullable = false)
  private Category category;

}
