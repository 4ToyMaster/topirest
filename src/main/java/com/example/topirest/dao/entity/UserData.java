package com.example.topirest.dao.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "userData")
public class UserData {

  @Id
  @GeneratedValue
  private Long id;

  private String firstName;
  private String middleName;
  private String lastName;
  private String login;
  private String password;
  private String email;

  @OneToMany(mappedBy = "userData", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
  private Set<TestResultItem> resultItems = new HashSet<TestResultItem>();

}
