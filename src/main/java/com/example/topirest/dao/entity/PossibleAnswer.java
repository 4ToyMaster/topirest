package com.example.topirest.dao.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "possible_answer")
public class PossibleAnswer {

  @Id
  @GeneratedValue
  Long id;

  String answer;
  boolean is_right;

  @ManyToOne
  @JoinColumn(name = "query_item_id", nullable = false)
  private QueryItem queryItem;
}
