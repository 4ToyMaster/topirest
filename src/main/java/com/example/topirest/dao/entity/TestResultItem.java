package com.example.topirest.dao.entity;

import java.math.BigDecimal;
import java.sql.Timestamp;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table( name = "testResultItem")
public class TestResultItem {
  @Id
  @GeneratedValue
  Long id;

  int mistakeCount;
  int rightCount;
  int countAll;
  Timestamp finishDate;
  BigDecimal percent;

  @ManyToOne
  @JoinColumn(name = "testItem_id", nullable = false)
  private TestItem testItem;

  @ManyToOne
  @JoinColumn(name = "user_data_id", nullable = false)
  private UserData userData;

}
