package com.example.topirest.dao.entity;

import java.util.HashSet;
import java.util.Set;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "query_item")
public class QueryItem {

  @Id
  @GeneratedValue
  Long id;

  String query;

  @OneToMany(mappedBy = "queryItem", cascade = CascadeType.ALL, fetch = FetchType.EAGER, orphanRemoval = true)
  private Set<PossibleAnswer> answers = new HashSet<>();

  @ManyToOne
  @JoinColumn(name = "testItem_id", nullable = false)
  private TestItem testItem;

}
