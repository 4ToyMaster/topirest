package com.example.topirest.dao.repository;

import com.example.topirest.dao.entity.LearningItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface LearningItemRepository extends JpaRepository<LearningItem, Long> {

}
