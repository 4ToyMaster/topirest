package com.example.topirest.dao.repository;

import com.example.topirest.dao.entity.TestResultItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestResultItemRepository extends JpaRepository<TestResultItem, Long> {

}
