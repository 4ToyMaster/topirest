package com.example.topirest.dao.repository;

import com.example.topirest.dao.entity.QueryItem;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface QueryItemRepository extends JpaRepository<QueryItem, Long> {

}
