package com.example.topirest.service;

import com.example.topirest.dao.entity.LearningItem;
import com.example.topirest.dao.repository.LearningItemRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class LearningItemService {

  @Autowired
  private LearningItemRepository repository;

  public List<LearningItem> getAll() {
    return repository.findAll();
  }

  public Optional<LearningItem> findById(Long id) {
    return repository.findById(id);
  }

  public LearningItem save (LearningItem LearningItem) {
    return repository.save(LearningItem);
  }

  public void deleteById (Long id) {
    repository.deleteById(id);
  }
}
