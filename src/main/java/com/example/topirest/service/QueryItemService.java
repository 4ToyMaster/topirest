package com.example.topirest.service;

import com.example.topirest.dao.entity.QueryItem;
import com.example.topirest.dao.repository.QueryItemRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class QueryItemService {

  @Autowired
  private QueryItemRepository repository;

  public List<QueryItem> getAll() {
    return repository.findAll();
  }

  public Optional<QueryItem> findById(Long id) {
    return repository.findById(id);
  }

  public QueryItem save (QueryItem QueryItem) {
    return repository.save(QueryItem);
  }

  public void deleteById (Long id) {
    repository.deleteById(id);
  }

}
