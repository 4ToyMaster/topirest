package com.example.topirest.service;

import com.example.topirest.dao.entity.UserData;
import com.example.topirest.dao.repository.UserDataRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDataService {

  @Autowired
  private UserDataRepository repository;

  public List<UserData> getAll() {
    return repository.findAll();
  }

  public Optional<UserData> findById(Long id) {
    return repository.findById(id);
  }

  public UserData save (UserData UserData) {
    return repository.save(UserData);
  }

  public void deleteById (Long id) {
    repository.deleteById(id);
  }

}
