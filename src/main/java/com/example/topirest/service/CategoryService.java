package com.example.topirest.service;

import com.example.topirest.dao.entity.Category;
import com.example.topirest.dao.repository.CategoryRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryService {

  @Autowired
  private CategoryRepository repository;

  public List<Category> getAll() {
    return repository.findAll();
  }

  public Optional<Category> findById(Long id) {
    return repository.findById(id);
  }

  public Category save (Category category) {
    return repository.save(category);
  }

  public void deleteById (Long id) {
    repository.deleteById(id);
  }

}
