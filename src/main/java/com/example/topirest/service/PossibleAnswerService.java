package com.example.topirest.service;

import com.example.topirest.dao.entity.PossibleAnswer;
import com.example.topirest.dao.repository.PossibleAnswerRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PossibleAnswerService {

  @Autowired
  private PossibleAnswerRepository repository;

  public List<PossibleAnswer> getAll() {
    return repository.findAll();
  }

  public Optional<PossibleAnswer> findById(Long id) {
    return repository.findById(id);
  }

  public PossibleAnswer save (PossibleAnswer PossibleAnswer) {
    return repository.save(PossibleAnswer);
  }

  public void deleteById (Long id) {
    repository.deleteById(id);
  }

}
