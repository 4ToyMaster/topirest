package com.example.topirest.service;

import com.example.topirest.dao.entity.TestResultItem;
import com.example.topirest.dao.repository.TestResultItemRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestResultItemService {

  @Autowired
  private TestResultItemRepository repository;

  public List<TestResultItem> getAll() {
    return repository.findAll();
  }

  public Optional<TestResultItem> findById(Long id) {
    return repository.findById(id);
  }

  public TestResultItem save (TestResultItem TestResultItem) {
    return repository.save(TestResultItem);
  }

  public void deleteById (Long id) {
    repository.deleteById(id);
  }

}
