package com.example.topirest.service;

import com.example.topirest.dao.entity.TestItem;
import com.example.topirest.dao.repository.TestItemRepository;
import java.util.List;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TestItemService {

  @Autowired
  private TestItemRepository repository;

  public List<TestItem> getAll() {
    return repository.findAll();
  }

  public Optional<TestItem> findById(Long id) {
    return repository.findById(id);
  }

  public TestItem save (TestItem TestItem) {
    return repository.save(TestItem);
  }

  public void deleteById (Long id) {
    repository.deleteById(id);
  }
}
